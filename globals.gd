extends Node

signal dialogue_started(dialogue: DialogueResource)
signal dialogue_ended(dialogue: DialogueResource)
