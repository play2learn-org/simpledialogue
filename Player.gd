extends CharacterBody2D

@export var speed : int = 600
@onready var movable : bool = true

func _ready() -> void:
	Globals.connect("dialogue_started", _pause)
	DialogueManager.connect("dialogue_ended", _unpause)

func input() -> void:
	if movable:
		var direction: Vector2 = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
		velocity = direction * speed
	else:
		velocity = Vector2(0,0)

func _physics_process(delta) -> void:
	input()
	move_and_slide()

func _pause(input) -> void:
	movable = false

func _unpause(input) -> void:
	movable = true
