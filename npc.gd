extends Area2D

@onready var active : bool = false
@export var dialogue: DialogueResource

func _ready() -> void:
	connect("body_entered", _body_entered)
	connect("body_exited", _body_exited)

func _body_entered(body) -> void:
	if body.name == "Player":
		active = true

func _body_exited(body) -> void:
	if body.name == "Player":
		active = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta) -> void:
	$Notifier.visible = active

func _unhandled_input(event) -> void:
	if Input.is_action_just_pressed("ui_accept") and active:
		DialogueManager.show_example_dialogue_balloon(dialogue, "start")
		Globals.emit_signal("dialogue_started", dialogue)
